# translation of kcmlaunch.po to Latvian
# Copyright (C) 2007, 2008 Free Software Foundation, Inc.
#
# Maris Nartiss <maris.kde@gmail.com>, 2007, 2008.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcmlaunch\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2008-07-04 16:06+0300\n"
"Last-Translator: Viesturs Zarins <viesturs.zarins@mii.lu.lv>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#. i18n: ectx: label, entry (cursorTimeout), group (BusyCursorSettings)
#. i18n: ectx: label, entry (taskbarTimeout), group (TaskbarButtonSettings)
#: launchfeedbacksettingsbase.kcfg:15 launchfeedbacksettingsbase.kcfg:29
#, kde-format
msgid "Timeout in seconds"
msgstr ""

#: package/contents/ui/main.qml:17
#, kde-format
msgid "Launch Feedback"
msgstr ""

#: package/contents/ui/main.qml:33
#, fuzzy, kde-format
#| msgid "Bus&y Cursor"
msgid "Cursor:"
msgstr "&Aizņemts kursors"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "No Feedback"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Static"
msgstr ""

#: package/contents/ui/main.qml:64
#, fuzzy, kde-format
#| msgid "Blinking Cursor"
msgid "Blinking"
msgstr "Mirgojošs kursors"

#: package/contents/ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Bouncing Cursor"
msgid "Bouncing"
msgstr "Lēkājošs kursors"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Task Manager:"
msgstr ""

#: package/contents/ui/main.qml:99
#, fuzzy, kde-format
#| msgid "Enable &taskbar notification"
msgid "Enable animation"
msgstr "Ieslēgt paziņojumu &uzdevumjoslā"

#: package/contents/ui/main.qml:113
#, fuzzy, kde-format
#| msgid "&Startup indication timeout:"
msgid "Stop animation after:"
msgstr "&Palaišanas paziņošanas noildze:"

#: package/contents/ui/main.qml:126 package/contents/ui/main.qml:138
#, fuzzy, kde-format
#| msgid " sec"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] " sek"
msgstr[1] " sek"
msgstr[2] " sek"

#~ msgid ""
#~ "<h1>Launch Feedback</h1> You can configure the application-launch "
#~ "feedback here."
#~ msgstr ""
#~ "<h1>Palaišanas paziņošana</h1> Šeit jūs varat konfigurēt programmu "
#~ "palaišanas paziņošanu."

#~ msgid ""
#~ "<h1>Busy Cursor</h1>\n"
#~ "KDE offers a busy cursor for application startup notification.\n"
#~ "To enable the busy cursor, select one kind of visual feedback\n"
#~ "from the combobox.\n"
#~ "It may occur, that some applications are not aware of this startup\n"
#~ "notification. In this case, the cursor stops blinking after the time\n"
#~ "given in the section 'Startup indication timeout'"
#~ msgstr ""
#~ "<h1>Aizņemts kursors</h1>\n"
#~ "KDE piedāvā aizņemtu kursoru programmu palaišanas paziņošanai.\n"
#~ "Lai ieslēgtu aizņemtu kursoru, atzīmējiet kādu no piedāvātajām iespējām.\n"
#~ "Var gadīties, ka programma nenojauš par palaišanas paziņošanu. Šajā "
#~ "gadījumā,\n"
#~ " kursors pārstās mirgot pēc laika, kāds norādīts\n"
#~ "sadaļā 'Palaišanas paziņošanas noildze'"

#~ msgid "No Busy Cursor"
#~ msgstr "Nav aizņemta kursora"

#~ msgid "Passive Busy Cursor"
#~ msgstr "Pasīvs aizņemts kursors"

#~ msgid "Taskbar &Notification"
#~ msgstr "&Paziņojums uzdevumjoslā"

#~ msgid ""
#~ "<H1>Taskbar Notification</H1>\n"
#~ "You can enable a second method of startup notification which is\n"
#~ "used by the taskbar where a button with a rotating hourglass appears,\n"
#~ "symbolizing that your started application is loading.\n"
#~ "It may occur, that some applications are not aware of this startup\n"
#~ "notification. In this case, the button disappears after the time\n"
#~ "given in the section 'Startup indication timeout'"
#~ msgstr ""
#~ "<H1>Paziņojums uzdevumjoslā</H1>\n"
#~ "Jūs varat atļaut otru palaišanas paziņojuma metodi, kura\n"
#~ "uzdevumjoslā parāda palaistās programmas ielādes simbolu.\n"
#~ "Var gadīties, ka programma nenojauš par palaišanas paziņošanu. Šajā "
#~ "gadījumā,\n"
#~ "simbols no uzdevumjoslas pazudīs pēc laika, kāds norādīts\n"
#~ "sadaļā 'Palaišanas paziņošanas noildze'"

#~ msgid "Start&up indication timeout:"
#~ msgstr "Palaišanas paziņošanas &noildze:"
