# Translation of kaccess to Norwegian Bokmål
#
# Hans Petter Bieker <bieker@kde.org>, 2000.
# Axel Bojer <fri_programvare@bojer.no>, 2005, 2006.
# Nils Kristian Tomren <slx@nilsk.net>, 2005.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2008, 2009, 2013, 2015.
msgid ""
msgstr ""
"Project-Id-Version: kaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-08 00:48+0000\n"
"PO-Revision-Date: 2015-08-27 16:58+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: kaccess.cpp:69
msgid ""
"The Shift key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Shift-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:70
msgid "The Shift key is now active."
msgstr "Shift-tasten er nå skrudd på."

#: kaccess.cpp:71
msgid "The Shift key is now inactive."
msgstr "Shift-tasten er nå skrudd av."

#: kaccess.cpp:75
msgid ""
"The Control key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Ctrl-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:76
msgid "The Control key is now active."
msgstr "Ctrl-tasten er nå skrudd på."

#: kaccess.cpp:77
msgid "The Control key is now inactive."
msgstr "Ctrl-tasten er nå skrudd av."

#: kaccess.cpp:81
msgid ""
"The Alt key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Alt-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:82
msgid "The Alt key is now active."
msgstr "Alt-tasten er nå skrudd på."

#: kaccess.cpp:83
msgid "The Alt key is now inactive."
msgstr "Alt-tasten er nå skrudd av."

#: kaccess.cpp:87
msgid ""
"The Win key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Win-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:88
msgid "The Win key is now active."
msgstr "Win-tasten er nå skrudd på."

#: kaccess.cpp:89
msgid "The Win key is now inactive."
msgstr "Win-tasten er nå skrudd av."

#: kaccess.cpp:93
msgid ""
"The Meta key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Meta-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:94
msgid "The Meta key is now active."
msgstr "Meta-tasten er nå skrudd på."

#: kaccess.cpp:95
msgid "The Meta key is now inactive."
msgstr "Meta-tasten er nå skrudd av."

#: kaccess.cpp:99
msgid ""
"The Super key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Super-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:100
msgid "The Super key is now active."
msgstr "Super-tasten er nå skrudd på."

#: kaccess.cpp:101
msgid "The Super key is now inactive."
msgstr "Super-tasten er nå skrudd av."

#: kaccess.cpp:105
msgid ""
"The Hyper key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Hyper-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:106
msgid "The Hyper key is now active."
msgstr "Hyper-tasten er nå skrudd på."

#: kaccess.cpp:107
msgid "The Hyper key is now inactive."
msgstr "Hyper-tasten er nå skrudd av."

#: kaccess.cpp:111
msgid ""
"The Alt Graph key has been locked and is now active for all of the following "
"keypresses."
msgstr ""
"Alt Gr-tasten er låst og kan nå brukes sammen med alle følgende tastetrykk."

#: kaccess.cpp:112
msgid "The Alt Graph key is now active."
msgstr "Alt Gr-tasten er nå skrudd på."

#: kaccess.cpp:113
msgid "The Alt Graph key is now inactive."
msgstr "Alt Gr-tasten er nå skrudd av."

#: kaccess.cpp:114
msgid "The Num Lock key has been activated."
msgstr "Num Lock-tasten er nå skrudd på."

#: kaccess.cpp:114
msgid "The Num Lock key is now inactive."
msgstr "Num Lock-tasten er nå skrudd av."

#: kaccess.cpp:115
msgid "The Caps Lock key has been activated."
msgstr "Caps Lock-tasten er nå skrudd på."

#: kaccess.cpp:115
msgid "The Caps Lock key is now inactive."
msgstr "Caps Lock-tasten er nå skrudd av."

#: kaccess.cpp:116
msgid "The Scroll Lock key has been activated."
msgstr "Scroll Lock-tasten er nå skrudd på."

#: kaccess.cpp:116
msgid "The Scroll Lock key is now inactive."
msgstr "Scroll Lock-tasten er nå skrudd av."

#: kaccess.cpp:332
#, kde-format
msgid "Toggle Screen Reader On and Off"
msgstr "Slå skjermleser på og av"

#: kaccess.cpp:334
#, kde-format
msgctxt "Name for kaccess shortcuts category"
msgid "Accessibility"
msgstr ""

#: kaccess.cpp:620
#, kde-format
msgid "AltGraph"
msgstr "Alt Graph"

#: kaccess.cpp:622
#, kde-format
msgid "Hyper"
msgstr "Hyper"

#: kaccess.cpp:624
#, kde-format
msgid "Super"
msgstr "Super"

#: kaccess.cpp:626
#, kde-format
msgid "Meta"
msgstr "Meta"

#: kaccess.cpp:643
#, kde-format
msgid "Warning"
msgstr "Advarsel"

#: kaccess.cpp:671
#, kde-format
msgid "&When a gesture was used:"
msgstr "&Når en musebevegelse ble brukt:"

#: kaccess.cpp:677
#, kde-format
msgid "Change Settings Without Asking"
msgstr "Endre innstillingene uten spørsmål"

#: kaccess.cpp:678
#, kde-format
msgid "Show This Confirmation Dialog"
msgstr "Vis dette vinduet for bekreftelse"

#: kaccess.cpp:679
#, kde-format
msgid "Deactivate All AccessX Features & Gestures"
msgstr "Skru av alle AccessX-funksjoner og musebevegelser"

#: kaccess.cpp:722 kaccess.cpp:724
#, kde-format
msgid "Slow keys"
msgstr "Trege taster"

#: kaccess.cpp:727 kaccess.cpp:729
#, kde-format
msgid "Bounce keys"
msgstr "Filtertaster"

#: kaccess.cpp:732 kaccess.cpp:734
#, kde-format
msgid "Sticky keys"
msgstr "Valgtaster"

#: kaccess.cpp:737 kaccess.cpp:739
#, kde-format
msgid "Mouse keys"
msgstr "Musetaster"

#: kaccess.cpp:746
#, kde-format
msgid "Do you really want to deactivate \"%1\"?"
msgstr "Er du sikker på at du vil skru av «%1»?"

#: kaccess.cpp:749
#, kde-format
msgid "Do you really want to deactivate \"%1\" and \"%2\"?"
msgstr "Er du sikker på at du vil skru av «%1» og «%2»?"

#: kaccess.cpp:753
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\" and \"%3\"?"
msgstr "Er du sikker på at du vil skru av «%1», «%2» og «%3»?"

#: kaccess.cpp:756
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "Er du sikker på at du vil skru av «%1», «%2», «%3» og «%4»?"

#: kaccess.cpp:767
#, kde-format
msgid "Do you really want to activate \"%1\"?"
msgstr "Er du sikker på at du vil skru på «%1»?"

#: kaccess.cpp:770
#, kde-format
msgid "Do you really want to activate \"%1\" and to deactivate \"%2\"?"
msgstr "Er du sikker på at du vil skru på «%1» og skru av «%2»?"

#: kaccess.cpp:773
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\" and \"%3\"?"
msgstr "Er du sikker på at du vil skru på «%1» og skru av «%2» og «%3»?"

#: kaccess.cpp:779
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\", \"%3\" and "
"\"%4\"?"
msgstr "Er du sikker på at du vil skru på «%1» og skru av «%2», «%3» og «%4»?"

#: kaccess.cpp:790
#, kde-format
msgid "Do you really want to activate \"%1\" and \"%2\"?"
msgstr "Er du sikker på at du vil skru på «%1» og «%2»?"

#: kaccess.cpp:793
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and \"%2\" and to deactivate \"%3\"?"
msgstr "Er du sikker på at du vil skru på «%1» og «%2» og skru av «%3»?"

#: kaccess.cpp:799
#, kde-format
msgid ""
"Do you really want to activate \"%1\", and \"%2\" and to deactivate \"%3\" "
"and \"%4\"?"
msgstr "Er du sikker på at du vil skru på «%1», «%2» og skru av «%3» og «%4»?"

#: kaccess.cpp:810
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\" and \"%3\"?"
msgstr "Er du sikker på at du vil skru på «%1», «%2» og «%3»?"

#: kaccess.cpp:813
#, kde-format
msgid ""
"Do you really want to activate \"%1\", \"%2\" and \"%3\" and to deactivate "
"\"%4\"?"
msgstr "Er du sikker på at du vil skru på «%1», «%2» og «%3» og skru av «%4»?"

#: kaccess.cpp:822
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "Er du sikker på at du vil skru på «%1», «%2», «%3» og «%4»?"

#: kaccess.cpp:831
#, kde-format
msgid "An application has requested to change this setting."
msgstr "Et program ba om å få endre dette valget."

#: kaccess.cpp:835
#, kde-format
msgid ""
"You held down the Shift key for 8 seconds or an application has requested to "
"change this setting."
msgstr ""
"Du holdt «Shift»-knappen nede i mer enn 8 sekunder eller et program ba om å "
"endre dette valget."

#: kaccess.cpp:837
#, kde-format
msgid ""
"You pressed the Shift key 5 consecutive times or an application has "
"requested to change this setting."
msgstr ""
"Du holdt «Shift»-knappen 5 ganger på rad eller et program ba om å endre "
"dette valget."

#: kaccess.cpp:841
#, kde-format
msgid "You pressed %1 or an application has requested to change this setting."
msgstr "Du trykket %1 eller et program ba om å endre dette valget."

#: kaccess.cpp:846
#, kde-format
msgid ""
"An application has requested to change these settings, or you used a "
"combination of several keyboard gestures."
msgstr ""
"Et program ba om å endre disse valgene, eller du brukte bestemte taste-"
"kombinasjoner."

#: kaccess.cpp:848
#, kde-format
msgid "An application has requested to change these settings."
msgstr "Et program ba om å endre disse valgene."

#: kaccess.cpp:853
#, kde-format
msgid ""
"These AccessX settings are needed for some users with motion impairments and "
"can be configured in the KDE System Settings. You can also turn them on and "
"off with standardized keyboard gestures.\n"
"\n"
"If you do not need them, you can select \"Deactivate all AccessX features "
"and gestures\"."
msgstr ""
"Disse AccessX-valgene behøves er nødvendig for noen brukere med "
"bevegelseshemmelser. Du kan endre valgene i KDE Systeminnstillinger. Du kan "
"også skru dem av eller på med standardiserte taste-kombinasjoner.\n"
"\n"
"Hvis du ikke har bruk for dem, kan du velge «skru av alle AccessX-funksjoner "
"og taste-kombinasjoner»."

#: kaccess.cpp:874
#, kde-format
msgid ""
"Slow keys has been enabled. From now on, you need to press each key for a "
"certain length of time before it gets accepted."
msgstr ""
"Trege taster er nå slått på. Fra nå av må du holde nede tastene en liten "
"stund før tastetrykket blir registrert."

#: kaccess.cpp:876
#, kde-format
msgid "Slow keys has been disabled."
msgstr "Trege taster er slått av."

#: kaccess.cpp:880
#, kde-format
msgid ""
"Bounce keys has been enabled. From now on, each key will be blocked for a "
"certain length of time after it was used."
msgstr ""
"Filtertaster er slått på. Fra nå av blir hver tast blokkert en liten stund "
"etter at den er brukt."

#: kaccess.cpp:882
#, kde-format
msgid "Bounce keys has been disabled."
msgstr "Filtertaster er slått av."

#: kaccess.cpp:886
#, kde-format
msgid ""
"Sticky keys has been enabled. From now on, modifier keys will stay latched "
"after you have released them."
msgstr ""
"Faste valgtaster er slått på. Fra nå av vil valgtastene kunne brukes selv "
"etter at du har sluppet dem."

#: kaccess.cpp:888
#, kde-format
msgid "Sticky keys has been disabled."
msgstr "Faste valgtaster er nå slått av."

#: kaccess.cpp:892
#, kde-format
msgid ""
"Mouse keys has been enabled. From now on, you can use the number pad of your "
"keyboard in order to control the mouse."
msgstr ""
"Musetastene er nå slått på. Fra nå av kan du styre musa med talltastaturet."

#: kaccess.cpp:894
#, kde-format
msgid "Mouse keys has been disabled."
msgstr "Mus-tastene er nå slått av."

#: main.cpp:49
#, kde-format
msgid "Accessibility"
msgstr ""

#: main.cpp:49
#, kde-format
msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgstr "© 2000 Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, kde-format
msgid "Author"
msgstr "Forfatter"
