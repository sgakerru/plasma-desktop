# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2014, 2016, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-21 00:46+0000\n"
"PO-Revision-Date: 2022-06-30 10:02+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.2\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Algemeen"

#: contents/ui/ConfigGeneral.qml:27
#, kde-format
msgid "Show active application's name on Panel button"
msgstr "Actieve toepassingsnaam op paneelknop tonen"

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Only icons can be shown when the Panel is vertical."
msgstr ""
"Pictogrammen kunnen alleen getoond worden wanneer het paneel verticaal is."

#: contents/ui/ConfigGeneral.qml:49
#, kde-format
msgid "Not applicable when the widget is on the Desktop."
msgstr "Niet toepasselijk wanneer het widget op het bureaublad is."

#: contents/ui/main.qml:80
#, kde-format
msgid "Plasma Desktop"
msgstr "Plasma Bureaublad"

#~ msgid "Show list of opened windows"
#~ msgstr "Lijst van geopende vensters tonen"

#~ msgid "On all desktops"
#~ msgstr "Op alle bureaubladen"

#~ msgid "Window List"
#~ msgstr "Vensterlijst"

#~ msgid "Actions"
#~ msgstr "Acties"

#~ msgid "Unclutter Windows"
#~ msgstr "Vensters ordenen"

#~ msgid "Cascade Windows"
#~ msgstr "Vensters trapsgewijs ordenen"

#~ msgctxt "%1 is the name of the desktop"
#~ msgid "Desktop %1"
#~ msgstr "Bureaublad %1"
