# Bosnian translation for bosnianuniversetranslation
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the bosnianuniversetranslation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: bosnianuniversetranslation\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-02 00:50+0000\n"
"PO-Revision-Date: 2015-01-04 13:07+0000\n"
"Last-Translator: Samir Ribić <Unknown>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 17341)\n"
"X-Launchpad-Export-Date: 2015-02-15 06:27+0000\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: package/contents/ui/main.qml:29
#, kde-format
msgid "Position on screen:"
msgstr ""

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Top"
msgstr ""

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Center"
msgstr ""

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Activation:"
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgctxt "@option:check"
msgid "Activate when pressing any key on the desktop"
msgstr ""

#: package/contents/ui/main.qml:69
#, kde-format
msgid "History:"
msgstr ""

#: package/contents/ui/main.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Remember past searches"
msgstr ""

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt "@option:check"
msgid "Retain last search when re-opening"
msgstr ""

#: package/contents/ui/main.qml:94
#, kde-format
msgctxt "@option:check"
msgid "Activity-aware (last search and history)"
msgstr ""

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Clear History"
msgstr ""

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@action:button %1 activity name"
msgid "Clear History for Activity \"%1\""
msgstr ""

#: package/contents/ui/main.qml:111
#, kde-format
msgid "Clear History…"
msgstr ""

#: package/contents/ui/main.qml:140
#, kde-format
msgctxt "@item:inmenu delete krunner history for all activities"
msgid "For all activities"
msgstr ""

#: package/contents/ui/main.qml:153
#, kde-format
msgctxt "@item:inmenu delete krunner history for this activity"
msgid "For activity \"%1\""
msgstr ""

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Plugins:"
msgstr ""

#: package/contents/ui/main.qml:174
#, fuzzy, kde-format
#| msgid "Configure Search"
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Konfiguriši pretragu"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Samir Ribić"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "samir.ribic@etf.unsa.ba"

#, fuzzy
#~| msgid "Configure Search"
#~ msgctxt "kcm name for About dialog"
#~ msgid "Configure search settings"
#~ msgstr "Konfiguriši pretragu"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#, fuzzy
#~| msgid "Select the search plugins"
#~ msgid "Select the search plugins:"
#~ msgstr "Odaberi dodatke za pretragu"
