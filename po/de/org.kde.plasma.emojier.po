# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Burkhard Lück <lueck@hube-lueck.de>, 2020, 2021.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-11 00:48+0000\n"
"PO-Revision-Date: 2022-07-23 23:55+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.07.70\n"

#: app/emojier.cpp:84 app/emojier.cpp:86
#, kde-format
msgid "Emoji Selector"
msgstr "Emoji-Auswahl"

#: app/emojier.cpp:88
#, kde-format
msgid "(C) 2019 Aleix Pol i Gonzalez"
msgstr "(C) 2019 Aleix Pol i Gonzalez"

#: app/emojier.cpp:90
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Deutsches KDE-Übersetzerteam"

#: app/emojier.cpp:90
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-i18n-de@kde.org"

#: app/emojier.cpp:105
#, kde-format
msgid "Replace an existing instance"
msgstr "Eine bestehende Instanz ersetzen"

#: app/ui/CategoryPage.qml:33 app/ui/emojier.qml:57
#, kde-format
msgid "Search"
msgstr "Suchen"

#: app/ui/CategoryPage.qml:74
#, kde-format
msgid "Clear History"
msgstr "Verlauf leeren"

#: app/ui/CategoryPage.qml:153
#, kde-format
msgid "No recent Emojis"
msgstr "Keine zuletzt verwendeten Emojis"

#: app/ui/emojier.qml:36
#, kde-format
msgid "%1 copied to the clipboard"
msgstr "%1 ist in die Zwischenablage kopiert worden"

#: app/ui/emojier.qml:47
#, kde-format
msgid "Recent"
msgstr "Zuletzt verwendet"

#: app/ui/emojier.qml:68
#, kde-format
msgid "All"
msgstr "Alle"

#: app/ui/emojier.qml:76
#, kde-format
msgid "Categories"
msgstr "Kategorien"

#: emojicategory.cpp:14
msgctxt "Emoji Category"
msgid "Smileys and Emotion"
msgstr ""

#: emojicategory.cpp:15
msgctxt "Emoji Category"
msgid "People and Body"
msgstr ""

#: emojicategory.cpp:16
msgctxt "Emoji Category"
msgid "Component"
msgstr ""

#: emojicategory.cpp:17
msgctxt "Emoji Category"
msgid "Animals and Nature"
msgstr ""

#: emojicategory.cpp:18
msgctxt "Emoji Category"
msgid "Food and Drink"
msgstr ""

#: emojicategory.cpp:19
msgctxt "Emoji Category"
msgid "Travel and Places"
msgstr ""

#: emojicategory.cpp:20
msgctxt "Emoji Category"
msgid "Activities"
msgstr ""

#: emojicategory.cpp:21
msgctxt "Emoji Category"
msgid "Objects"
msgstr ""

#: emojicategory.cpp:22
msgctxt "Emoji Category"
msgid "Symbols"
msgstr ""

#: emojicategory.cpp:23
msgctxt "Emoji Category"
msgid "Flags"
msgstr ""

#~ msgid "Search…"
#~ msgstr "Suchen …"

#~ msgid "Search..."
#~ msgstr "Suchen ..."
