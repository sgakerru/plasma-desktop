# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# giovanni <g.sora@tiscali.it>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2021-10-07 22:49+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.2\n"

#: contents/config/config.qml:6
#, kde-format
msgid "General"
msgstr "General"

#: contents/ui/configGeneral.qml:23
#, kde-format
msgid "Display style:"
msgstr "Stilo de monstrator:"

#: contents/ui/configGeneral.qml:63
#, kde-format
msgctxt "@info:placeholder Make this translation as short as possible"
msgid "No flag available"
msgstr ""

#: contents/ui/configGeneral.qml:104
#, kde-format
msgid "Layouts:"
msgstr "Dispositiones:"

#: contents/ui/configGeneral.qml:105
#, kde-format
msgid "Configure…"
msgstr "Configura…"

#~ msgid "Show label"
#~ msgstr "Monstra etiquetta"

#~ msgid "Show flag"
#~ msgstr "Monstra bandiera"
